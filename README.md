# README #

1/3スケールのm5風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- ソード
- タカラ

## 発売時期
- m5 1982年11月
- ゲームパソコン 1982年11月
- ゲームパソコンM5 1983年
- m5 Pro 1983年11月

## 参考資料

- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/784701435212550144)
- [Wikipedia](https://ja.wikipedia.org/wiki/M5_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [IPSJ コンピュータ博物館](http://museum.ipsj.or.jp/computer/personal/0089.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_m5/raw/c4d8304764d1ce2c2897f81aa96828285af0f48b/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_m5/raw/c4d8304764d1ce2c2897f81aa96828285af0f48b/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_m5/raw/c4d8304764d1ce2c2897f81aa96828285af0f48b/ExampleImage.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_m5/raw/c4d8304764d1ce2c2897f81aa96828285af0f48b/ExampleImage_2.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_m5/raw/c4d8304764d1ce2c2897f81aa96828285af0f48b/ExampleImage_3.jpg)
